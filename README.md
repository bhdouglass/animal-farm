# Animal Farm

A simple game for babies to touch animals and hear them make noises on Ubuntu Touch.

This a fork of the [Animal Farm app by Robert Ancell](https://launchpad.net/animal-farm).

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/animal-farm.bhdouglass)

## Development

Build and run using [clickable](https://github.com/bhdouglass/clickable).

## Translations

Translations are handled online via [Weblate](https://translate-ut.org/projects/animal-farm/animal-farm/).
A huge thank you to all the translators!

## License

Copyright (C) 2019 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
